# Intelligent-Conversational-System-using-Python-NLTK

## Description

The purpose of this project is to find train journey through a dialogue and then find the cheapest available train ticket for the journey through data collected by the national railway inquiry systems.

## Dependencies

* NLTK
* Experta
* BeautifulSoup4 
* Requests 
* Stanford-ner-2020-11-17
